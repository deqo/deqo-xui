import { EmojioneProvider } from './Emojione';
import { EmojiCategoryData, EmojiData, EmojiProvider } from './Model';


const store = {
    rebuild: true,
    providers: [ new EmojioneProvider() ] as EmojiProvider[],
    codeDict: {} as { [code:string]:string },
    emojiIndex: {} as { [id:string]:EmojiData },
    emojiList: [] as EmojiData[],
    categoryList: [] as EmojiCategoryData[],
};

export const EmojiStore = {

    register: (ep:EmojiProvider) => {
        store.providers.push(ep);
        store.rebuild = true;
    },

    getEmojis: () => {
        checkRebuild();
        return store.emojiList as Readonly<EmojiData[]>;
    },

    getCategories: () => {
        checkRebuild();
        return store.categoryList as Readonly<EmojiCategoryData[]>;
    },

    lookup: (idOrCode:string) => {
        checkRebuild();
        idOrCode = store.codeDict[idOrCode] || idOrCode;
        return (store.emojiIndex[idOrCode] || null) as EmojiData;
    },

};

function checkRebuild() {

    if (!store.rebuild) return;

    store.emojiList = store.providers.map(x => x.getEmojis()).reduce((t, x) => t.concat(x), []).sort((a, b) => a.order - b.order);
    store.categoryList = store.providers.map(x => x.getCategories()).reduce((t, x) => t.concat(x), []).sort((a, b) => a.order - b.order);

    store.codeDict = {};
    store.emojiIndex = {};

    for (const e of store.emojiList) {
        store.emojiIndex[e.id] = e;
        for (const c of e.codes) {
            store.codeDict[c] = e.id;
        }
    }

    store.rebuild = false;
}