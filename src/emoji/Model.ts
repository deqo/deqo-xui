

export interface EmojiProvider {
    getEmojis():EmojiData[];
    getCategories():EmojiCategoryData[];
}

export interface EmojiData {

    readonly id:string;
    readonly label:string;
    readonly codes:string[];
    readonly keywords:string[];
    readonly order:number;
    readonly ascii:string[];
    readonly category:string;
    readonly diversity:string;
    readonly canDiversify:boolean;
}

export interface EmojiCategoryData {
    readonly id:string;
    readonly label:string;
    readonly order:number;
}

export interface EmojiDivisityData {
    readonly id:string;
    readonly code:string;    
    readonly order:number;
}

export const EmojiDiversifications:EmojiDivisityData[] = [
    {
        id: "default skin tone",
        code: null,
        order: 0,
    },
    {
        id: "light skin tone",
        code: "1f3fb",
        order: 1,
    },
    {
        id: "medium-light skin tone",
        code: "1f3fc",
        order: 2,
    },
    {
        id: "medium skin tone",
        code: "1f3fd",
        order: 3,
    },
    {
        id: "medium-dark skin tone",
        code: "1f3fe",
        order: 4,
    },
    {
        id: "dark skin tone",
        code: "1f3ff",
        order: 5,
    },
];

