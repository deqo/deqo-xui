import { EmojiCategoryData, EmojiData, EmojiProvider } from './Model';


const emojiJson = require('emojione/emoji.json');
const categoryJson = require('emojione/categories.json');

export const EmojioneEmojis:Readonly<EmojiData[]> = (() => {
    const list = [] as EmojiData[];
    for (const id in emojiJson) {
        const x = emojiJson[id];
        list.push({
            id: id,
            label: x.name,
            codes: [x.shortname].concat(x.shortname_alternates),
            keywords: ([x.shortname].concat(x.shortname_alternates).concat(x.keywords)).map(x => x.replace(/[\s\:\-\_]/g, '')),
            order: x.order,
            ascii: x.ascii,
            category: x.category,
            diversity: x.diversity,
            canDiversify: !!x.diversities && x.diversities.length > 0,
        });
    }
    list.sort((a, b) => a.order - b.order);
    return list;
})();

export const EmojioneCategories:Readonly<EmojiCategoryData[]> = (() => {
    const list = [] as EmojiCategoryData[];
    for (const x of categoryJson) {
        list.push({
            id: x.category,
            label: x.category_label,
            order: x.order,
        });
    }
    list.sort((a, b) => a.order - b.order);
    return list;
})();

export class EmojioneProvider implements EmojiProvider {
    public getEmojis = () => EmojioneEmojis;
    public getCategories = () => EmojioneCategories;
}