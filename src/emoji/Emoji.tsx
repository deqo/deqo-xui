import * as React from 'react';

import { EmojiStore } from './EmojiStore';


export const Emoji:React.SFC<{ idOrCode:string, inline?:boolean, type?:string}> = p => {
    const e = EmojiStore.lookup(p.idOrCode) || EmojiStore.lookup(':skull:');
    return React.createElement(p.type || 'span', { className: `emoji ${p.inline ? 'emoji-inline' : ''} emoji-${e.id}`});
}
