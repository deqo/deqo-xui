import { EmojiStore } from './EmojiStore';


export function emojiHtml(code:string, inline?:boolean) {
    const e = EmojiStore.lookup(code);
    const classes= ['emoji', !!inline ? 'emoji-inline' : '', 'emoji-' + e.id].join(' ');
    if (e) return `<span class="${classes}"></span>`;
    throw 'Invalid emoji code: ' + code;
}