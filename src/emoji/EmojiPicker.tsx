import * as React from 'react';
import Scrollbars from 'react-custom-scrollbars';

import { Emoji } from './Emoji';
import { EmojiStore } from './EmojiStore';
import { EmojiData, EmojiDiversifications } from './Model';

require('./res/emoji-picker.scss');
require('./res/emojione.scss');


export type EmojiPickerProps = { width?:number, height?:number, onSelect?:(emoji:EmojiData)=>void };
export type EmojiPickerState = { category:string, diversity:string, preview?:string, search?:string };


export class EmojiPicker extends React.Component<EmojiPickerProps, EmojiPickerState> {

    static defaultProps = {
        width: 300,
        height: 295,
    }

    private headingMap:{[name:string]:number} = {};
    private scrollbars:Scrollbars = null;

    private contents:EmojiPickerContents;

    constructor(props:EmojiPickerProps) {
        super(props);

        const category = EmojiStore.getCategories()[0].id;
        const diversity = EmojiDiversifications[0].code

        this.state = { category, diversity };
        this.contents = computeContents(diversity);
    }

    public render() {
        const state = this.state;
        const categories = EmojiStore.getCategories();
        const diversities = EmojiDiversifications;

        return <div className="emoji-picker">
            <div className="emoji-category-list">
                {categories.map(x =>
                    <div className={'emoji-category-list-item ' + (x.id === state.category ? '--active' : '')}>
                        <div className={'emoji-category-' + x.id} 
                             onClick={() => this.onCategoryClick(x.id)} />
                    </div>
                )}
            </div>
            <div className="emoji-search">
                <input type="text" placeholder="Search" value={this.state.search} onChange={e => this.onSearch(e.currentTarget.value)} />
            </div>
            <div className="emoji-options">
                {diversities.map(x =>
                    <div key={x.id} 
                         className={cl('emoji-diversity', `emoji-diversity-${x.code || 'default'}`, [x.code === state.diversity, '--active'])} 
                         onClick={() => this.onDiversitySelect(x.code)}
                    />
                )}
            </div>
            <Scrollbars ref={e => this.scrollbars = e} 
                        onScroll={() => this.onScroll()}
                        className="emoji-picker-container" 
                        style={this.props}>
                <div className="emoji-picker-container">
                    <div className="emoji-list">
                        {this.contents.map(x => <>
                            <div ref={e => this.headingMap[x.key] = !!e ? e.offsetTop : 0} className="emoji-category-heading">{x.label}</div>
                            {x.items.map(x => 
                                <div className="emoji-list-item" 
                                     onMouseEnter={() => this.onEmojiPreview(true, x.id)} 
                                     onMouseLeave={() => this.onEmojiPreview(false, x.id)} 
                                     onClick={() => this.onEmojiSelect(x.id)}>
                                    <Emoji idOrCode={x.id} />
                                </div>
                            )}
                        </>)}                  
                    </div>
                </div>
            </Scrollbars>
            <EmojiPreview idOrCode={state.preview} width={this.props.width} />
        </div>
    }

    public componentDidMount = () => this.onScroll();

    private onCategoryClick(category:string):void {
        const { headingMap, scrollbars} = this;
        scrollbars.scrollTop(headingMap[category]);
    }

    private onScroll():void {
        const { headingMap, scrollbars} = this;

        let category = null;
        let top = null

        for (let name in headingMap) {
            const p = headingMap[name];
            if (p <= scrollbars.getScrollTop()) {
                if (!top || top < p) {
                    top = p;
                    category = name;
                }
            }
        }

        this.setState({ category });
    }

    private onSearch(search:string):void {
        this.contents = computeContents(this.state.diversity, search );
        this.setState({ search });
    }

    private onEmojiPreview(active:boolean, emoji:string):void {
        if (active) {
            this.setState({ preview: emoji });
        }
        else if (this.state.preview === emoji) {
            this.setState({ preview: null });
        }
    }

    private onEmojiSelect(emoji:string):void {
        const onSelect = this.props.onSelect || (() => {});
        onSelect(EmojiStore.lookup(emoji));
    }

    private onDiversitySelect(diversity:string):void {
        this.contents = computeContents(diversity, this.state.search);
        this.setState({ diversity });
    }
}

const EmojiPreview = ({idOrCode, width}) => {
    const emoji = EmojiStore.lookup(idOrCode);
    return <div className="emoji-preview" style={{width}} title={!!emoji ? emoji.label : null}>
        {emoji && <>
            <Emoji idOrCode={emoji.id} inline /> {emoji.label}
        </>}
    </div>
}

type EmojiPickerContents = Array<{ key:string, label:string, items:EmojiData[] }>;

function computeContents(diversity:string, query?:string):EmojiPickerContents {

    query = (query || '').trim().toLowerCase();

    let es = EmojiStore.getEmojis();
    let cs = EmojiStore.getCategories();

    //Reduce...
    es = es.filter(x => x.diversity === diversity || (!x.canDiversify && !x.diversity));
    if (query) {
        es = es.filter(x => x.keywords.some(w => w.startsWith(query)));
    }

    //Group...
    const escs = es.reduce((r, v, i, a, k = v.category) => ((r[k] || (r[k] = [])).push(v), r), {});

    //Assemble...
    const hs = cs
        .map(x => ({
            key: x.id,
            label: x.label,
            items: escs[x.id] || [],
        }))
        .filter(x => !!x.items.length);

    return hs;
}

function cl(...input:any[]) {
    input = input.map(x => Array.isArray(x) ? (!!x[0] ? x[1] : '') : x);
    return input.join(' ');
}