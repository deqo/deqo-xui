const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const outputTo = path.resolve(__dirname, '../dist')

const baseConfig = {
    entry: './src/index.ts',    
    devtool: 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: ['file-loader?name=[name].[ext]']
            }            
        ]
    },
    plugins: [
        new CleanWebpackPlugin([outputTo], { allowExternal: true }),
    ],    
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
};

module.exports = { outputTo, baseConfig };