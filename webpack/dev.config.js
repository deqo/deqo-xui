const merge = require('webpack-merge');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { outputTo, baseConfig } = require('./base.config.js');

module.exports = merge(baseConfig, {
    mode: 'development',
    devServer: {
        contentBase: outputTo,
        compress: true,
        port: 8080
    },
    module: {
        rules: [
            {
                test: /\.s?css$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({ 
            title: 'deqo-xui',
        }),
    ]
});