import { EmojiCategoryData, EmojiData, EmojiProvider } from './Model';
export declare const EmojiStore: {
    register: (ep: EmojiProvider) => void;
    getEmojis: () => EmojiData[];
    getCategories: () => EmojiCategoryData[];
    lookup: (idOrCode: string) => EmojiData;
};
