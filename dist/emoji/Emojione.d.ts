import { EmojiCategoryData, EmojiData, EmojiProvider } from './Model';
export declare const EmojioneEmojis: Readonly<EmojiData[]>;
export declare const EmojioneCategories: Readonly<EmojiCategoryData[]>;
export declare class EmojioneProvider implements EmojiProvider {
    getEmojis: () => EmojiData[];
    getCategories: () => EmojiCategoryData[];
}
