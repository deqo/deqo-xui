export interface EmojiProvider {
    getEmojis(): EmojiData[];
    getCategories(): EmojiCategoryData[];
}
export interface EmojiData {
    readonly id: string;
    readonly label: string;
    readonly codes: string[];
    readonly keywords: string[];
    readonly order: number;
    readonly ascii: string[];
    readonly category: string;
    readonly diversity: string;
    readonly canDiversify: boolean;
}
export interface EmojiCategoryData {
    readonly id: string;
    readonly label: string;
    readonly order: number;
}
export interface EmojiDivisityData {
    readonly id: string;
    readonly code: string;
    readonly order: number;
}
export declare const EmojiDiversifications: EmojiDivisityData[];
