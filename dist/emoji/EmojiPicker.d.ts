import * as React from 'react';
import { EmojiData } from './Model';
export declare type EmojiPickerProps = {
    width?: number;
    height?: number;
    onSelect?: (emoji: EmojiData) => void;
};
export declare type EmojiPickerState = {
    category: string;
    diversity: string;
    preview?: string;
    search?: string;
};
export declare class EmojiPicker extends React.Component<EmojiPickerProps, EmojiPickerState> {
    static defaultProps: {
        width: number;
        height: number;
    };
    private headingMap;
    private scrollbars;
    private contents;
    constructor(props: EmojiPickerProps);
    render(): JSX.Element;
    componentDidMount: () => void;
    private onCategoryClick;
    private onScroll;
    private onSearch;
    private onEmojiPreview;
    private onEmojiSelect;
    private onDiversitySelect;
}
