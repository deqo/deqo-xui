import * as React from 'react';
export declare const Emoji: React.SFC<{
    idOrCode: string;
    inline?: boolean;
    type?: string;
}>;
